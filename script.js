const gameContainer = document.getElementById("game");

// const COLORS = [
//   "red",
//   "blue",
//   "green",
//   "orange",
//   "purple",
//   "red",
//   "blue",
//   "green",
//   "orange",
//   "purple"
// ];

const gifArray = ["gifs/1.gif", "gifs/2.gif", "gifs/3.gif", "gifs/4.gif", "gifs/5.gif", "gifs/6.gif"
  , "gifs/7.gif", "gifs/8.gif", "gifs/9.gif", "gifs/10.gif", "gifs/11.gif", "gifs/12.gif", "gifs/1.gif", "gifs/2.gif", "gifs/3.gif", "gifs/4.gif", "gifs/5.gif", "gifs/6.gif"
  , "gifs/7.gif", "gifs/8.gif", "gifs/9.gif", "gifs/10.gif", "gifs/11.gif", "gifs/12.gif"];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffled_gifArray = shuffle(gifArray);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card

function createDivsForColors(gifArray) {
  for (let gif of gifArray) {
    // create a new div
    const newDiv = document.createElement("div");
    // newDiv.classList.add("card");
    const imgEl = document.createElement("img");
    imgEl.src = gif
    newDiv.style.height = "5.5em";
    newDiv.style.width = "5.5em";
    newDiv.style.borderRadius = "10px";
    // newDiv.style.border="solid 2px pink";
    newDiv.style.backgroundColor = "purple";
    imgEl.style.height = "100%";
    imgEl.style.width = "100%";
    imgEl.style.borderRadius = "10px";
    imgEl.style.opacity = 0;
    newDiv.classList.add("card");

    // give it a class attribute for the value we are looping over
    // newDiv.classList.add(color);
    newDiv.appendChild(imgEl)

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);

    // game_play_container.append(newDiv);
  }
}

//show image
function show(event) {
  event.target.style.opacity = "1";
}

//remove event listener
function remove_event_listener(correctimages) {

  correctimages[0].parentElement.removeEventListener("click", handleCardClick);
  correctimages[1].parentElement.removeEventListener("click", handleCardClick);
}

//updating score
function updateScore(score) {
  let scoreEl = document.getElementById("score");
  scoreEl.innerHTML = `score :${score}`;

}

//adding score 
function score_add() {
  let score = 0;

  let h1_El = document.createElement("h1");
  h1_El.id = "score";
  h1_El.innerText = `score :${score}`;
  // document.body.append(h1_El);
  // game_play_container.append(h1_El);
  score_timeout.append(h1_El);
}





// TODO: Implement this function!
let count = 0
let firstImageSrc = "";
let secondImageSrc = "";
let total_click = 0;
let score = 0;
function handleCardClick(event) {

  // you can use event.target to see which element was clicked
  console.log("you clicked", event.target);

  total_click++;
  console.log(total_click);
  if (total_click < 3) {
    event.target.classList.add("clicked");
    show(event);
    if (count === 0) {
      firstImageSrc = event.target.getAttribute("src");
      count++;
    } else {
      secondImageSrc = event.target.getAttribute("src");
      count = 0;

      if (firstImageSrc === secondImageSrc && (firstImageSrc) && (secondImageSrc)) {
        const correctimages = document.querySelectorAll("img[src='" + firstImageSrc + "']");
        console.log("hi", correctimages);

        remove_event_listener(correctimages);
        total_click = 0;
        correctimages[0].classList.add("checked");
        correctimages[0].classList.remove("clicked");
        correctimages[1].classList.add("checked");
        correctimages[1].classList.remove("clicked");
        score++;
        updateScore(score);

      } else {
        const incorrectImages = document.querySelectorAll(".clicked");
        console.log("hello", incorrectImages);
        incorrectImages[0].classList.add("shake");
        incorrectImages[0].classList.remove("clicked");
        incorrectImages[1].classList.add("shake");
        incorrectImages[1].classList.remove("clicked");
        setTimeout(() => {
          incorrectImages[0].style.opacity = "0";
          incorrectImages[1].style.opacity = "0";
          total_click = 0;
        }, 1000);
      }
      if (secondImageSrc) {
        firstImageSrc = "";
        secondImageSrc = "";
      }
    }
    console.log(firstImageSrc);
    console.log(secondImageSrc);
    console.log(count);
  }
}

function first(start, game_container, game_over) {

  let game_heading = document.createElement("div");
  game_heading.innerText = "Match-A-gif";
  game_heading.id = "match_a_game"
  console.log("hello");
  //start button
  let div_btn = document.createElement("div");
  // div_btn.id = "start-btn";
  let btn1 = document.createElement("button");
  btn1.id = "start-btn";
  btn1.innerHTML = "Start";
  btn1.style.width = "8rem";
  btn1.style.height = "3rem";
  btn1.style.backgroundColor = "lightblue";
  btn1.style.borderRadius = "15px";
  btn1.addEventListener("click", () => {
    hidden(start);
    remove_hidden(game_play_container);
    start_time();

  });
  div_btn.append(btn1);
  start.append(game_heading, div_btn);
  game_container.classList.add("hidden");
  game_over.classList.add("hidden");
  hidden(game_container);
  hidden(game_over_container);


}

function starts_game(game_play_container) {
  score_add();

  game_play_container.append(score_timeout);
  createDivsForColors(shuffled_gifArray);
  game_play_container.append(gameContainer);
  // appendbutton();
}

let start_container = document.createElement("div");
let game_play_container = document.createElement("div");
let game_over_container = document.createElement("div");
function three_div_creation() {

  start_container.classList.add("start");
  game_play_container.classList.add("game_play");
  game_over_container.classList.add("game_over");
  starts_game(game_play_container);
  first(start_container, game_play_container, game_over_container);

  document.body.append(start_container, game_play_container, game_over_container);


}
function hidden(element) {
  element.classList.add("hidden");
  element.style.display = "none";
}

function remove_hidden(element) {
  element.classList.remove("hidden");
  element.style.display = "block";
}



//div for score and time
let score_timeout = document.createElement("div");
score_timeout.id = "score_timout_container";

// function start(){
// score_add();
// createDivsForColors(gifArray);

// }

//start button
// let div_btn = document.createElement("div");
// div_btn.id = "start";
// let btn1 = document.createElement("button");
// btn1.innerHTML = "Start";
// btn1.style.width = "50%";
// btn1.style.maxWidth = "300px";
// btn1.style.height = "6vh";
// btn1.style.padding = "10px;"
// btn1.style.backgroundColor = "lightblue";
// btn1.style.borderRadius = "15px";
// btn1.addEventListener("click",()=>{

//   //added a timer element

let timeEl = document.createElement("h3");
timeEl.id = "timer";
// document.body.append(timeEl);
score_timeout.append(timeEl);

function start_time() {
  let time_out = 60;
  //changing time in every second
  let interval_Id = setInterval(function () {
    time_out--;
    let timeEl_start = document.getElementById("timer");
    console.log(timeEl_start);
    timeEl_start.innerText = `Time:${time_out}`;
    // document.body.append(timeEl_start);
    score_timeout.append(timeEl_start);

    if (time_out === 0) {

      clearInterval(interval_Id);
      let final_score = document.getElementById("score");
      let new_score = final_score.innerText.split(":");
      console.log(new_score);
      let your_scoreEL = document.createElement("div");
      let game_over_head = document.createElement("div");
      game_over_head.id = "game_over_text";
      your_scoreEL.id = "end_score";
      game_over_head.innerText = "Game Over!"
      your_scoreEL.innerText = `Your score :${new_score[1]}`;
      // document.body.append(your_scoreEL);
      game_over_container.append(game_over_head, your_scoreEL);
      hidden(game_play_container);
      remove_hidden(game_over_container);

    }

  }, 1000);
}
// });

// document.body.appendChild(btn1);

//reset button
// let div_btn = document.createElement("div");
// div_btn.id = "reset";
let btn2 = document.createElement("button");
btn2.id = "reset"
btn2.innerHTML = "reset";
btn2.style.width = "50%";
btn2.style.maxWidth = "300px";
btn2.style.height = "6vh";
btn2.style.margin = "10px;";
btn2.style.borderRadius = "15px";
btn2.style.backgroundColor = "lightblue";
btn2.addEventListener("click", () => {
  location.reload();
  hidden(start);
  remove_hidden(game_play_container);
  start_time();
});

function appendbutton() {
  game_play_container.append(btn2);
}
// document.body.appendChild(btn2);

//main task
// let shuffledColors = shuffle(COLORS);

//create three div with diferent class
// let shuffledColors = shuffle(gifArray);

// score_add();
// createDivsForColors(gifArray);
three_div_creation();